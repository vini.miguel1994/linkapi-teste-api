import mongoose from 'mongoose';


const bookSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    description: {
        type: String
    },
    price: {
        type: Number,
        required: true
    },
    author: String
},{
    collection: 'book'
});

export default mongoose.model('Books', bookSchema);

