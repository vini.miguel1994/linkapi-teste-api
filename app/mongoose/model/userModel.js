import mongoose from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    hidden: true
  },
  email: {
    type: String,
    required: true,
    index: true,
    unique: true
  }
}, {
  collection: 'user'
});

userSchema.pre('save', function(next) {
  const user = this;
  if (!user.isModified('password')) {
    return next();
  }
  bcrypt.genSalt(5, function (err, salt) {
    if (err) {
      return next(err);
    }
    bcrypt.hash(user.password, salt, null, function (err, hash)  {
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.authenticate = function (password, next) {
  bcrypt.compare(password, this.password, (err, isMatch) => {
    if (err) {
      return next(err);
    }
    next(isMatch);
  });
};

export default mongoose.model('User', userSchema);

