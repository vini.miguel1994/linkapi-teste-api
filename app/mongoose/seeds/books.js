import Books from '../model/bookModel';
import _ from 'lodash';

const books = [
    {
        title: 'Google Android',
        description: 'Aprenda a criar aplicações para despositivos moveis',
        price: 190.00,
        author: 'Ricardo R. Lecheta'
    },
    {
        title: 'Complexidade de algritimos',
        description: 'Análise, projeto e métodos',
        price: 100.00,
        author: 'Laira Vieira Toscani'
    },
    {
        title: 'REST',
        description: 'Contrua APIs inteligentes de maneira simples',
        price: 69.90,
        author: 'Alexandre Saudate'
    },
    {
        title: 'JPA Eficaz',
        description: 'As melhores práticas de persistência de dados em Java',
        price: 69.90,
        author: 'Hébert Coelho'
    },
    {
        title: 'jQuery, a bibliote do programado JavaScript',
        description: 'Aprenda a criar efeitos de alto impacto em seu site',
        price: 120.90,
        author: 'Maurício Samy Silva'
    },
];

_.each(books, book => {
    new Books(book).save().catch(() => {});
});
