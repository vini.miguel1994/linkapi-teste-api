import Books from '../../mongoose/model/bookModel';

const BooksController = {
  getBooks: (req, res) => {
    Books.find({}, (err, data) => {
      if (err) {
        res.status(400).end(err);
      } else {
        res.json(data);
      }
    });
  },

  getBook: (req, res) => {
    const name = req.params.name;
    Books.findOne({name}, (err, data) => {
      if (err) {
        res.status(400).end(err);
      } else if (data === null){
        res.json({message: 'Nenhum livro encontrado'});
      } else {
        res.json(data);
      }
    });
  },

  newBook: (req, res) => {
    const title = req.body.title;
    const description = req.body.description;
    const price = req.body.price;
    const author = req.body.author;

    const newBook = new Books({
      title,
      description,
      price,
      author
    });

    newBook.save((err) => {
      if (err) {
        res.status(400).send(err);
      } else {
        res.json({message: 'Novo livro criado com sucesso', data: 'ok'});
      }
    });
  },
  removeBook: (req, res) => {
    const title = req.params.title;
    Books.remove({title}, (err) => {
      if (err) {
        res.status(400).send(err);
      } else {
        res.json({message: `Livro ${title} removido com sucesso`, data: 'ok'});
      }
    });
  }
};

export default BooksController;

