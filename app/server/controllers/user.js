import User from '../../mongoose/model/userModel';

const userController = {
  createUser: (req, res) => {
    const newUser = new User({
      name: req.body.name,
      password: req.body.password,
      email: req.body.email
    });
    newUser.save((err, data) => {
      if (err) {
        res.send(err);
      } else {
        res.json({message: 'Novo usuário', data});
      }
    });
  }
};

export default userController;
