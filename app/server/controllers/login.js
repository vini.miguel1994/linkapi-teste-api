import User from '../../mongoose/model/userModel';
import jwt from 'jsonwebtoken';

const secret = process.env.SECRET_TOKEN;

const LoginController = {
  login : (req, res) => {
    const userPassword = req.body.password;
    const userEmail = req.body.email;
    if (!userEmail || !userPassword) {
      return res.status(400).end();
    }
    User.findOne({email: userEmail}, (err, user) => {
      if (err) {
        return res.status(400).end();
      }
      if (user) {
        user.authenticate(userPassword, (plainTextPassword) => {
          if (!plainTextPassword){
            return res.status(400).send({message: 'Senha incorreta'});
          }
          const token = jwt.sign({data: user}, secret, {
            expiresIn: 60
          });
  
          res.json({
            success: true,
            message: 'Token ativado',
            token
          });
        });
      } else {
        res.status(404).json({
          success: false,
          message: 'Usuario não encontrado'
        });
      }
    });
  }
};

export default LoginController;

