import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import route from './router';
import jwt from 'jsonwebtoken';

const secret = process.env.SECRET_TOKEN;
const app = express();
const port = process.env.PORT || 8080;
const routeExpress = express.Router();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use('/api',routeExpress);

routeExpress.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

routeExpress.use(function(req, res, next) {
      const token = req.body.token || req.headers['x-access-token'];
      if (token) {
        jwt.verify(token, secret, function (err, decoded) {
          if (err) {
            return res.json({
              success: false,
              message: 'Falha ao autenticar TOKEN'
            });
          } else {
            req.decoded = decoded;
            next();
          }
        });
      } else {
        return res.status(403).send({
          success: false,
          message: 'Não há token'
        });
      }
    });

route(routeExpress, app);

app.listen(port, () => {
  console.log(`Server iniciado em http://localhost:${port}`);
  console.log('CTRL + C derruba o server');
});

export default app;

