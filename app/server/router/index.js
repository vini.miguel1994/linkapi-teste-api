import LoginController from '../controllers/login';
import BooksController from '../controllers/books';
import UserController from '../controllers/user';

const router = (routeExpress, app) => {

  routeExpress.get('/books', BooksController.getBooks);
  routeExpress.get('/books/:name', BooksController.getBook);
  
  app.post('/login', LoginController.login);
  app.post('/createUser', UserController.createUser);
};

export default router;

