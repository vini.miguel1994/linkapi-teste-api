# Teste  FCamara API

### Sobre

A API foi criada em NodeJs , Express e MongoDB.
Autenticação realizada via JWT.

### Instalação

* Rode o serviço do  MongoDB
* Instalando dependencias

```
npm install
```
* Rodando Servidor

```
npm start
```

### Usando

Para que seja possível visualizar os livros , é necessário efetuar o login para que seja gerado o Token.

Após o Token de acesso ser gerado, devera enviar nos headers da requisição o Token com a chave : "x-access-token" e assim será possível ter acesso aos produtos(Livros).

A API vem com um usuário pré cadastrado:

```
email: vini.miguel1994@gmail.com
senha: linkapiteste2017
```
Mas caso seja necessário criar um usuário, a rota "/createUser" recebe as chaves:
```
name,
email,
password
```
E o cadastro é realizado.
