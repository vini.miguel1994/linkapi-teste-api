import 'dotenv/config';
import './app';
import './app/mongoose/seeds/user';
import './app/mongoose/seeds/books';
